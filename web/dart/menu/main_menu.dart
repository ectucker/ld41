part of game;

class MainMenu extends Menu {

  MainMenu(var game): super(game);

  @override
  void bind() {
    subscribe(querySelector('#start').onClick.listen(startGame));
  }

  @override
  void create() {
    buildFromHtml(game.assetManager.get('main.html'));
    game.assetManager.get('menu.ogg').loop();
  }

  @override
  preload() {
    game.assetManager.load('main.html', HttpRequest.getString('html/main.html'));
    game.assetManager.load('hud.html', HttpRequest.getString('html/hud.html'));
    game.assetManager.load('instructions.html', HttpRequest.getString('html/instructions.html'));
    game.assetManager.load('gameover.html', HttpRequest.getString('html/gameover.html'));
  }

  void startGame(e) {
    globalGame.assetManager.get('sub.ogg').play();
    game.pushState(new GameState(game));
  }

  void instructions(e) {
    game.pushState(new InstructionMenuState(game));
  }

  @override
  show() {
    super.show();
  }

  @override
  hide() {
    super.hide();
  }

}

class MainMenuState extends MenuState {

  Game game;

  MainMenuState(this.game) : super(new MainMenu(game)) {

  }

  @override
  create() {
    super.create();
  }

  @override
  render(num delta) {
    game.gl.clearScreen(Colors.black);
  }

  @override
  resize(num width, num height) {
  }

  @override
  update(num delta) {

  }

}