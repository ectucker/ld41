part of game;

class EndMenu extends Menu {

  EndMenu(var game): super(game);

  @override
  void bind() {
    subscribe(querySelector('#start').onClick.listen(startGame));
  }

  @override
  void create() {
    game.assetManager.get('ending.ogg').loop();
    buildFromHtml(game.assetManager.get('gameover.html'));
  }

  @override
  preload() {

  }

  void startGame(e) {
    game.pushState(new GameState(game));
    game.assetManager.get('ending.ogg').stop();
  }

  void instructions(e) {
    game.pushState(new InstructionMenuState(game));
  }

  @override
  show() {
    super.show();
  }

  @override
  hide() {
    super.hide();
  }

}

class EndMenuState extends MenuState {

  Game game;
  int score;

  EndMenuState(this.game, this.score) : super(new EndMenu(game)) {}

  @override
  create() {
    super.create();
    querySelector('#score').innerHtml = "Final Score: " + score.toString();
  }

  @override
  render(num delta) {
    game.gl.clearScreen(Colors.black);
  }

  @override
  resize(num width, num height) {
  }

  @override
  update(num delta) {

  }

}