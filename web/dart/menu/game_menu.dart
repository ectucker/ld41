part of game;

class GameMenu extends Menu {

  GameMenu(var game): super(game);

  @override
  void bind() {

  }

  @override
  void create() {
    buildFromHtml(game.assetManager.get('hud.html'));
  }

  @override
  preload() {

  }

  void startGame(e) {
    game.pushState(new GameState(game));
  }

  @override
  show() {
    super.show();
  }

  @override
  hide() {
    super.hide();
  }

}