part of game;

class InstructionMenu extends Menu {

  InstructionMenu(var game): super(game);

  @override
  void bind() {
    subscribe(querySelector('#back').onClick.listen(mainMenu));
  }

  @override
  void create() {
    buildFromHtml(game.assetManager.get('instructions.html'));
  }

  @override
  preload() {}

  void mainMenu(e) {
    game.pushState(new MainMenuState(game));
  }

  @override
  show() {
    super.show();
  }

  @override
  hide() {
    super.hide();
  }

}

class InstructionMenuState extends MenuState {

  Game game;

  InstructionMenuState(this.game) : super(new InstructionMenu(game)) {

  }

  @override
  create() {
    super.create();
  }

  @override
  render(num delta) {
    game.gl.clearScreen(Colors.lightGoldenrodYellow);
  }

  @override
  resize(num width, num height) {
  }

  @override
  update(num delta) {

  }

}