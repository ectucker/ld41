part of game;

class Card extends Component {

  List<Entity> hand;

  String type;
  int level;
  double roundmult;

  String image;

  bool used;

  Card(this.hand, this.type, this.level, this.roundmult, this.image);

  Card.basic(var hand): this(hand, 'ribbon', 1, 1.2, 'ribbon1');

  Card.fromType(var hand, var type): this(hand, type['type'], type['level'], type['roundmult'], type['image']);

  setFromType(var type) {
    this.type = type['type'];
    this.level = type['level'];
    this.roundmult = type['roundmult'];
    this.image = type['image'];
    this.used = false;
  }

}

pickCard(int round) {
  int choice;
  var key;
  if(round <= 16) {
    choice = rand.nextInt(cardtypes['deck'][round].length);
    key = cardtypes['deck'][round][choice];
  } else {
    choice = rand.nextInt(cardtypes['deck'][100].length);
    key = cardtypes['deck'][100][choice];
  }
  if(key == 'chBoss' || key == 'shBoss') {
    globalGame.assetManager.get('curse.ogg').play();
  }
  return cardtypes['cards'][key];
}