part of game;

Random rand = new Random();

Vector2 randPos() {
  if(rand.nextBool()) {
    if(rand.nextBool()) {
      return new Vector2(1920.0 + 400.0, rand.nextInt(1080).toDouble());
    } else {
      return new Vector2(-400.0, rand.nextInt(1080).toDouble());
    }
  } else {
    if(rand.nextBool()) {
      return new Vector2(rand.nextInt(1920).toDouble(), -400.0);
    } else {
      return new Vector2(rand.nextInt(1920).toDouble(), 1080.0 + 400.0);
    }
  }
}

Entity makeEnemy(var world, String type, int level) {
  var enemyData = enemytypes[type];
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position.fromVec(randPos()));
  enemy.addComponent(new StaticTexture(atlas[enemyData['image'] + "1"]));
  enemy.addComponent(new Hitbox(enemyData['hitbox'][0], enemyData['hitbox'][1],
      enemyData['hitbox'][2], enemyData['hitbox'][3]));
  enemy.addComponent(new Velocity(0.0, 0.0));
  enemy.addComponent(new Collision());
  enemy.addComponent(new Health(enemyData[level]['hp'], false));
  enemy.addComponent(new PointValue(enemyData[level]['points']));
  enemy.addComponent(new Enemy());
  enemy.addComponent(new EnemyData.fromMap(enemyData[level]));
  if(enemyData['shooting']) {
    enemy.addComponent(new ShootingEnemyAI());
  } else {
    enemy.addComponent(new BasicEnemyAI());
  }
  enemy.addComponent(new Rotation.none());
  enemy.addComponent(new Animation(enemyData['image'], enemyData['frames'], enemyData['duration']));
  enemy.addToWorld();
}

void placeEnemiesForCard(Card card, World world, int rounds) {
  if(card.type == 'chthulhu') {
    makeCthulu(world);
    globalGame.assetManager.get('combat.ogg').stop();
    globalGame.assetManager.get('boss.ogg').loop();
  } else if(card.type == 'shark') {
    makeSharkBoss(world);
    globalGame.assetManager.get('combat.ogg').stop();
    globalGame.assetManager.get('boss.ogg').loop();
  } else {
    globalGame.assetManager.get('menu.ogg').stop();
    globalGame.assetManager.get('combat.ogg').loop();
    for (int i = 0; i < (card.roundmult * rounds).ceil(); i++) {
      makeEnemy(world, card.type, card.level);
    }
  }
}

void applyEffectsForCard(Card card, PlayerControlSystem player) {
  if(card.type == 'attack') {
    player.damage *= 1.5;
  } else if(card.type == 'speed') {
    player.speed *= 1.25;
  } else if(card.type == 'fire') {
    player.fireRate *= 0.75;
  }
}

Entity makeSharkBoss(var world) {
  var enemyData = enemytypes['shark'];
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position(rand.nextInt(1920), 650));
  enemy.addComponent(new StaticTexture(atlas[enemyData['image'] + "1"]));
  enemy.addComponent(new Hitbox(enemyData['hitbox'][0], enemyData['hitbox'][1],
      enemyData['hitbox'][2], enemyData['hitbox'][3]));
  enemy.addComponent(new Velocity(0.0, 0.0));
  enemy.addComponent(new Collision());
  enemy.addComponent(new Health(enemyData[1]['hp'], false));
  enemy.addComponent(new PointValue(enemyData[1]['points']));
  enemy.addComponent(new Enemy());
  enemy.addComponent(new EnemyData.fromMap(enemyData[1]));
  enemy.addComponent(new SharkAI());
  enemy.addComponent(new Rotation.none());
  enemy.addComponent(new Animation(enemyData['image'], enemyData['frames'], enemyData['duration']));
  enemy.addComponent(new Flip.none());
  enemy.addToWorld();
}

Entity makeCthulu(var world) {
  var enemyData = enemytypes['chthulhu'];
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position(920, 0));
  enemy.addComponent(new StaticTexture(atlas[enemyData['image'] + "1"]));
  enemy.addComponent(new Hitbox(enemyData['hitbox'][0], enemyData['hitbox'][1],
      enemyData['hitbox'][2], enemyData['hitbox'][3]));
  enemy.addComponent(new Velocity(0.0, 0.0));
  enemy.addComponent(new Collision());
  enemy.addComponent(new Health(enemyData[1]['hp'], false));
  enemy.addComponent(new PointValue(enemyData[1]['points']));
  enemy.addComponent(new Enemy());
  enemy.addComponent(new EnemyData.fromMap(enemyData[1]));
  enemy.addComponent(new CthuluAI());
  enemy.addComponent(new Rotation.none());
  enemy.addComponent(new Flip.none());
  enemy.addComponent(new Animation(enemyData['image'], enemyData['frames'], enemyData['duration']));
  enemy.addToWorld();
}