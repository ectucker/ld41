part of game;

class Flip extends Component {

  bool x;
  bool y;

  Flip(this.x, this.y);

  Flip.none(): this(false, false);

}

class Rotation extends Component {

  double value;

  Rotation(this.value);

  Rotation.none(): this(0.0);

}