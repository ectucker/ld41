part of game;

class Hitbox extends Component {

  double offsetX, offsetY;
  double width, height;

  Aabb2 box;

  Hitbox(this.offsetX, this.offsetY, this.width, this.height) {
    box = new Aabb2.centerAndHalfExtents(new Vector2(offsetX, offsetY), new Vector2(width / 2, height / 2));
  }

}