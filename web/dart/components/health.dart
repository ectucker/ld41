part of game;

class Health extends Component {

  double value;
  bool player;

  Health(this.value, this.player);

}

class Damage extends Component {

  double value;
  bool player;

  Damage(this.value, this.player);

}

class PointValue extends Component {

  int value;

  PointValue(this.value);

}