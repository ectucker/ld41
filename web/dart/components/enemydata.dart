part of game;

class EnemyData extends Component {

  double health;
  double damage;
  double attackrate;
  double speed;
  int points;

  EnemyData(this.health, this.damage, this.attackrate, this.speed, this.points);

  EnemyData.fromMap(var map): this(map['hp'], map['damage'], map['attackrate'], map['speed'], map['points']);

}