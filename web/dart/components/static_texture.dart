part of game;

class StaticTexture extends Component {

  Texture texture;

  StaticTexture(this.texture);

}

class Scale extends Component implements Tweenable {

  static const int SCALE = 1;

  double value;

  Scale(this.value);

  int getTweenableValues(Tween tween, int tweenType, List<num> returnValues) {
    if (tweenType == SCALE) {
      returnValues[0] = value;
    }
    return 1;
  }

  void setTweenableValues( Tween tween, int tweenType, List<num> newValues) {
    if (tweenType == SCALE) {
      value = newValues[0];
    }
  }

}