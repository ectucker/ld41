part of game;

Entity makePlayer(var world) {
  Entity player = world.createEntity();
  player.addComponent(new Position(960.0, 540.0));
  player.addComponent(new StaticTexture(atlas['player']));
  player.addComponent(new Hitbox(0.0, 0.0, 128.0, 129.0));
  player.addComponent(new Velocity(0.0, 0.0));
  player.addComponent(new PlayerControl());
  player.addComponent(new Collision());
  player.addComponent(new Health(100.0, true));
  player.addComponent(new Rotation.none());
  player.addToWorld();
  return player;
}

Entity makeMissile(var world, Vector2 position, Vector2 direction, double damage, num rotation) {
  globalGame.assetManager.get('shoot.ogg').play();

  Entity bullet = world.createEntity();
  bullet.addComponent(new Position(position.x, position.y));
  bullet.addComponent(new StaticTexture(atlas['bullet']));
  bullet.addComponent(new Hitbox(0.0, 0.0, 80.0, 32.0));
  bullet.addComponent(new Velocity.fromVec(direction));
  bullet.addComponent(new EdgeDelete());
  bullet.addComponent(new Collision());
  bullet.addComponent(new Damage(damage, true));
  bullet.addComponent(new RemoveOnCollision());
  bullet.addComponent(new Rotation(rotation));
  bullet.addToWorld();
  return bullet;
}

Entity makeLightning(var world, Vector2 position, Vector2 direction, double damage, num rotation) {
  Entity bullet = world.createEntity();
  bullet.addComponent(new Position(position.x, position.y));
  bullet.addComponent(new StaticTexture(atlas['lightning']));
  bullet.addComponent(new Hitbox(0.0, 0.0, 80.0, 32.0));
  bullet.addComponent(new Velocity.fromVec(direction));
  bullet.addComponent(new EdgeDelete());
  bullet.addComponent(new Collision());
  bullet.addComponent(new Damage(damage, false));
  bullet.addComponent(new RemoveOnCollision());
  bullet.addComponent(new Rotation(rotation));
  bullet.addComponent(new Enemy());
  bullet.addToWorld();
  return bullet;
}

Entity makeMouseCoordTest(var world) {
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position(0.0, 0.0));
  enemy.addComponent(new StaticTexture(atlas['bullet']));
  enemy.addComponent(new FollowMouse());
  enemy.addToWorld();
  return enemy;
}

Entity makeDeck(var world) {
  Entity deck = world.createEntity();
  deck.addComponent(new Position(50.0, 50.0));
  deck.addComponent(new StaticTexture(atlas['cardback']));
  deck.addComponent(new Scale(0.5));
  deck.addToWorld();
  return deck;
}

Entity makeDiscard(var world) {
  Entity deck = world.createEntity();
  deck.addComponent(new Position(1920 - 263 * 0.5 - 50.0, 50.0));
  deck.addComponent(new StaticTexture(atlas['cardback']));
  deck.addComponent(new Scale(0.5));
  deck.addToWorld();
  return deck;
}

Entity drawCard(var world, var hand, int n) {
  double sectionWidth = (1920 - 400) / 5;
  double x = 200 + n * sectionWidth + (sectionWidth - 263) / 2;
  double y = 1080 / 2 - 368 / 2;

  Card cardData = new Card.fromType(hand, pickCard(1));

  Entity card = world.createEntity();
  card.addComponent(new Position(50.0, 50.0));
  card.addComponent(new StaticTexture(atlas[cardData.image]));
  card.addComponent(new Scale(0.5));
  card.addComponent(new Hitbox(0.0, 0.0, 263.0, 368.0));
  card.addComponent(cardData);
  card.addToWorld();

  CardSelectSystem cardSelect = world.getSystem(CardSelectSystem);
  new Tween.call((n, b) => cardSelect.cardsDrawn = true)
    ..delay = 2.75
    ..start(globalGame.tweenManager);

  Position position = card.getComponentByClass(Position);
  new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
    ..delay = 2.0
    ..targetValues = [x, y]
    ..easing = Sine.INOUT
    ..start(globalGame.tweenManager);

  Scale scale = card.getComponentByClass(Scale);
  new Tween.to(scale, Scale.SCALE, 0.75)
    ..delay = 2.0
    ..targetValues = [1.0]
    ..easing = Sine.INOUT
    ..start(globalGame.tweenManager);

  return card;
}

Entity enemyPrimer(var world) {
  Entity enemy = world.createEntity();
  enemy.addComponent(new Enemy());
  enemy.addComponent(new InstaDeath());
  enemy.addToWorld();
}

Entity makeBackground(var world) {
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position(0.0, 0.0));
  enemy.addComponent(new StaticTexture(atlas['background']));
  enemy.addToWorld();
}