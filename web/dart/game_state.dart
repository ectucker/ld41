part of game;

class GameState extends MenuState {

  Game game;
  World world;

  List hand = [];

  Camera2D camera;

  GameState(this.game): super(new GameMenu(game)) {
    world = new World();
  }

  @override
  create() {
    super.create();

    makeBackground(world);
    var player = makePlayer(world);

    var discard = makeDiscard(world);

    world.addSystem(new RoundControlSystem(discard, hand, player));
    world.addSystem(new PlayerControlSystem(keyboard, mouse));
    world.addSystem(new CardSelectSystem(mouse, discard));
    world.addSystem(new BasicAISystem(player));
    world.addSystem(new ShootingAISystem(player));
    world.addSystem(new SharkAISystem(player));
    //world.addSystem(new SerpantAISystem(player));
    world.addSystem(new CthuluAISystem(player));
    world.addSystem(new MovementSystem());
    world.addSystem(new MouseFollowSystem(mouse));
    world.addSystem(new HitboxPositionSystem());
    world.addSystem(new EdgeDeleteSystem());
    world.addSystem(new HitboxPositionSystem());
    world.addSystem(new CollisionListSystem());
    world.addSystem(new DamageSystem());
    world.addSystem(new DeathSystem());
    world.addSystem(new CollisionRemoveSystem());
    //world.addSystem(new CameraCenterSystem());
    world.addSystem(new AnimationSystem());
    world.addSystem(new RenderSystem(gl, width, height, canvas));

    globalGame.assetManager.get('deal.ogg').play();
    for(int i = 0; i < 5; i++) {
      hand.add(drawCard(world, hand, i));
    }
    makeDeck(world);

    world.initialize();
  }

  @override
  pause() {}

  @override
  preload() {}

  @override
  render(num delta) {
    world.delta = delta;
    world.process();
  }

  @override
  resize(num width, num height) {
    RenderSystem system = world.getSystem(RenderSystem);
    system.resize(width, height, canvas);
  }

  @override
  resume() {}

  @override
  update(num delta) {}

}
