part of game;

Map<String, Texture> atlas;
var enemytypes;
var cardtypes;

class Game extends BaseGame {

  State state, push;

  pushState(State state) {
    this.state?.pause();
    push = state;
    push.preload();
  }

  @override
  create() {
    atlas = assetManager.get('atlas');
    enemytypes = assetManager.get('enemytypes.yaml');
    cardtypes = assetManager.get('cards.yaml');
    gl.setGLViewport(canvasWidth, canvasHeight);
    pushState(new MainMenuState(this));
    resize(width, height);
  }

  @override
  preload() {
    assetManager.load("atlas", loadAtlas("img/atlas.json", loadTexture(gl, "img/atlas.png", mipMap)));
    assetManager.load("enemytypes.yaml", loadYaml("enemytypes.yaml"));
    assetManager.load("cards.yaml", loadYaml("cards.yaml"));
    assetManager.load("atlas", loadAtlas("img/atlas.json", loadTexture(gl, "img/atlas.png", linear)));
    assetManager.load("water", loadProgram(gl, "glsl/basic.vert", "glsl/water.frag"));
    assetManager.load("lighting", loadProgram(gl, "glsl/basic.vert", "glsl/lighting.frag"));
    assetManager.load("shoot.ogg", loadSound(audio, "sound/shoot.ogg"));
    assetManager.load("sub.ogg", loadSound(audio, "sound/sub.ogg"));
    assetManager.load("deal.ogg", loadSound(audio, "sound/deal2.ogg"));
    assetManager.load("boost.ogg", loadSound(audio, "sound/boost.ogg"));
    assetManager.load("cardplay.ogg", loadSound(audio, "sound/cardplay.ogg"));
    assetManager.load("menu.ogg", loadMusic(audio, "sound/menu.ogg"));
    assetManager.load("combat.ogg", loadMusic(audio, "sound/fighter.ogg"));
    assetManager.load("boss.ogg", loadMusic(audio, "sound/boss.ogg"));
    assetManager.load("playerhit.ogg", loadSound(audio, "sound/playerhit.ogg"));
    assetManager.load("hit.ogg", loadSound(audio, "sound/hit.ogg"));
    assetManager.load("curse.ogg", loadSound(audio, "sound/curse.ogg"));
    assetManager.load("ending.ogg", loadSound(audio, "sound/ending.ogg"));
  }

  @override
  update(num delta) {
    if (assetManager.allLoaded()) {
      push?.create();
      push?.resume();
      state = push ?? state;
      push = null;
    }

    state?.update(delta);
  }

  @override
  render(num delta) {
    gl.clearScreen(Colors.black);

    state?.render(delta);
  }

  @override
  pause() {
    state?.pause();
  }

  @override
  resume() {
    state?.resume();
  }

  @override
  resize(width, height) {
    gl.setGLViewport(canvasWidth, canvasHeight);

    state?.resize(width, height);

    DivElement ui = querySelector('#ui');
    num scale = min(window.innerWidth / 1920, window.innerHeight / 1080);
    ui.style.width = (1920 * scale).toString() + "px";
    ui.style.height = (1080 * scale).toString() + "px";
  }

  @override
  config() {
    scaleMode = ScaleMode.fit;
    requestedWidth = 1920;
    requestedHeight = 1080;
  }

}
