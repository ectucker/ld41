part of game;

class CameraCenterSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<CameraFollow> cameraMapper;

  CameraCenterSystem() : super(Aspect.getAspectForAllOf([Position, CameraFollow])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    cameraMapper = new Mapper<CameraFollow>(CameraFollow, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];

    RenderSystem render = world.getSystem(RenderSystem);
    render.camera.setTranslation(-position.x, -position.y);
  }

}