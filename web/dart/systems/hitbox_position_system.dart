part of game;

class HitboxPositionSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Scale> scaleMapper;

  HitboxPositionSystem() : super(Aspect.getAspectForAllOf([Position, Hitbox])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    scaleMapper = new Mapper<Scale>(Scale, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];

    double scaleVal = 1.0;
    if(scaleMapper.has(entity)) {
      Scale scale = scaleMapper[entity];
      scaleVal = scale.value;
    }

    hitbox.box.setCenterAndHalfExtents(
        new Vector2(position.x + hitbox.width / 2 * scaleVal + hitbox.offsetX, position.y + hitbox.height / 2 * scaleVal + hitbox.offsetY),
        new Vector2(hitbox.width / 2 * scaleVal, hitbox.height / 2 * scaleVal));
  }

}