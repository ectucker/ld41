part of game;

class PlayerControlSystem extends EntityProcessingSystem {

  Keyboard keyboard;
  Mouse mouse;
  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Rotation> rotationMapper;

  double speed = 400.0;
  double fireRate = 0.2;
  double damage = 10.0;

  double fireTime = 0.2;

  bool stopped = true;

  PlayerControlSystem(this.keyboard, this.mouse) : super(Aspect.getAspectForAllOf([Velocity, PlayerControl, Hitbox])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Velocity velocity = velocityMapper[entity];
    Position position = positionMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];
    if(!stopped) {
      if (keyboard.keyPressed(KeyCode.W)) {
        velocity.y = speed;
      } else if (keyboard.keyPressed(KeyCode.S)) {
        velocity.y = -speed;
      } else {
        velocity.y = 0.0;
      }
      if (keyboard.keyPressed(KeyCode.A)) {
        velocity.x = -speed;
      } else if (keyboard.keyPressed(KeyCode.D)) {
        velocity.x = speed;
      } else {
        velocity.x = 0.0;
      }

      Vector2 direction = render.mouseToCanvas(
          new Vector2(mouse.x.toDouble(), mouse.y.toDouble()));
      direction.sub(hitbox.box.center.clone());
      direction.normalize();
      direction.scale(500.0);

      num rot = atan2(direction.y, direction.x) + PI;

      fireTime -= world.delta;
      if (mouse.leftDown && fireTime <= 0.0) {
        makeMissile(world, hitbox.box.center.clone(), direction, damage, rot);
        fireTime = fireRate;
      }

      rotationMapper[entity].value = rot;
    } else {
      velocity.xy = new Vector2.all(0.0);
    }
  }

}