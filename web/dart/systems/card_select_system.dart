part of game;

class CardSelectSystem extends EntityProcessingSystem {

  Mouse mouse;

  Mapper<Hitbox> hitboxMapper;
  Mapper<Card> cardMapper;

  bool cardsDrawn = false;

  Entity discard;

  CardSelectSystem(this.mouse, this.discard) : super(Aspect.getAspectForAllOf([Card, Hitbox])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    cardMapper = new Mapper<Card>(Card, world);
  }

  @override
  void processEntity(Entity entity) {
    RoundControlSystem rounds = world.getSystem(RoundControlSystem);
    RenderSystem render = world.getSystem(RenderSystem);
    Hitbox hitbox = hitboxMapper[entity];
    Card cardData = cardMapper[entity];

    if(mouse.leftDown && cardMapper.has(entity)) {
      if(hitbox.box.containsVector2(render.mouseToCanvas(new Vector2(mouse.x.toDouble(), mouse.y.toDouble()))) && rounds.selecting && !cardData.used && cardsDrawn) {
        globalGame.assetManager.get('cardplay.ogg').play();
        PlayerControlSystem system = world.getSystem(PlayerControlSystem);
        if(['attack', 'speed', 'fire'].contains(cardData.type)) {
          applyEffectsForCard(cardData, system);
          cardData.used = true;
          cardsDrawn = false;
          rounds.redrawCard();
        } else {
          cardsDrawn = false;
          placeEnemiesForCard(cardData, world, rounds.round);
          system.stopped = false;
          for (var card in cardData.hand) {
            Position position = card.getComponentByClass(Position);
            if (card != entity)
              new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
                ..targetValues = [50.0, 50.0]
                ..easing = Sine.INOUT
                ..start(globalGame.tweenManager);
            else {
              new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
                ..targetValues = [1920 - 263 * 0.5 - 50.0, 50.0]
                ..easing = Sine.INOUT
                ..start(globalGame.tweenManager);
            }

            Scale scale = card.getComponentByClass(Scale);
            new Tween.to(scale, Scale.SCALE, 0.75)
              ..targetValues = [0.5]
              ..easing = Sine.INOUT
              ..start(globalGame.tweenManager);
          }
          rounds.selecting = false;
          cardData.used = true;
        }
        StaticTexture discardTexture = discard.getComponentByClass(StaticTexture);
        final String currentImage = cardData.image;
        new Tween.call((n, b) => discardTexture.texture = atlas[currentImage])
          ..delay = 0.75
          ..easing = Sine.INOUT
          ..start(globalGame.tweenManager);
      }
    }
  }

}