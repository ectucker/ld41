part of game;

class CthuluAISystem extends EntityProcessingSystem {

  Entity player;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Health> healthMapper;
  Mapper<EnemyData> enemyMapper;

  double fireTime = 5.0;
  double spawnTime = 2.0;
  int aiDir = 1;

  CthuluAISystem(this.player) : super(Aspect.getAspectForAllOf([CthuluAI, EnemyData])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    healthMapper = new Mapper<Health>(Health, world);
    enemyMapper = new Mapper<EnemyData>(EnemyData, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Velocity velocity = velocityMapper[entity];
    Position position = positionMapper[entity];
    EnemyData enemyData = enemyMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];
    Position playerPos = positionMapper[player];

    fireTime -= world.delta;
    if(fireTime <= 0.0) {
      makeTentacle(world, playerPos.x);
      fireTime = enemyData.attackrate;
    }
  }

}

Entity makeTentacle(var world, double x) {
  var enemyData = enemytypes['tentacle'];
  final Entity enemy = world.createEntity();
  final Position position = new Position(x, -1000);
  enemy.addComponent(position);
  enemy.addComponent(new StaticTexture(atlas[enemyData['image'] + "1"]));
  enemy.addComponent(new Hitbox(enemyData['hitbox'][0], enemyData['hitbox'][1],
      enemyData['hitbox'][2], enemyData['hitbox'][3]));
  enemy.addComponent(new Collision());
  enemy.addComponent(new Health(enemyData[1]['hp'], false));
  enemy.addComponent(new PointValue(enemyData[1]['points']));
  enemy.addComponent(new Enemy());
  enemy.addComponent(new EnemyData.fromMap(enemyData[1]));
  enemy.addComponent(new Damage(enemyData[1]['damage'], false));
  enemy.addComponent(new Rotation.none());
  enemy.addComponent(new Animation(enemyData['image'], enemyData['frames'], enemyData['duration']));
  enemy.addComponent(new RemoveOnCollision());
  enemy.addToWorld();

  new Tween.to(position.vec, Vector2Accessor.XY, 2.0)
    ..targetValues = [x, 0.0]
    ..easing = Bounce.IN
    ..start(globalGame.tweenManager);
}