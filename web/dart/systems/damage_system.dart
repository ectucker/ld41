part of game;

class DamageSystem extends EntityProcessingSystem {

  Mapper<Health> healthMapper;
  Mapper<Damage> damageMapper;
  Mapper<Collision> collisionMapper;

  DamageSystem() : super(Aspect.getAspectForAllOf([Collision, Health])) {}

  @override
  void initialize() {
    healthMapper = new Mapper<Health>(Health, world);
    collisionMapper = new Mapper<Collision>(Collision, world);
    damageMapper = new Mapper<Damage>(Damage, world);
  }

  @override
  void processEntity(Entity entity) {
    Collision collision = collisionMapper[entity];
    Health health = healthMapper[entity];

    for(Entity other in collision.others) {
      if(damageMapper.has(other)) {
        if(damageMapper[other].player != health.player) {
          health.value -= damageMapper[other].value;
          if(!health.player) {
            globalGame.assetManager.get('hit.ogg').play();
          }
        }
      }
    }
  }

}