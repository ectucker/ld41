part of game;

class CollisionListSystem extends EntitySystem {

  Mapper<Hitbox> hitboxMapper;
  Mapper<Collision> collisionMapper;

  CollisionListSystem() : super(Aspect.getAspectForAllOf([Hitbox, Collision])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    collisionMapper = new Mapper<Collision>(Collision, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    for(Entity entity in entities) {
      collisionMapper[entity].others.clear();

      for(Entity other in entities) {
        Hitbox entityBox = hitboxMapper[entity];
        Hitbox otherBox = hitboxMapper[other];
        if(entityBox.box.intersectsWithAabb2(otherBox.box) && otherBox.box != entityBox.box) {
          collisionMapper[entity].others.add(other);
        }
      }
    }
  }

}
