part of game;

class RoundControlSystem extends EntitySystem implements Tweenable {

  static const int SCORE = 1;
  static const int HEALTH = 2;

  Mapper<Health> healthMapper;

  var scoreElement;
  var healthElement;

  Entity discard;
  List<Entity> hand;

  Entity player;

  int round = 1;
  bool selecting = true;

  int _score = 0;
  double _lastHealth = 100.0;

  double displayScore = 0.0;
  double displayHealth = 100.0;

  RoundControlSystem(this.discard, this.hand, this.player) : super(Aspect.getAspectForAllOf([Enemy])) {}

  @override
  void initialize() {
    scoreElement = querySelector("#score");
    healthElement = querySelector("#hp");
    healthMapper = new Mapper<Health>(Health, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    if(healthMapper[player].value != _lastHealth) {
      if(healthMapper[player].value < _lastHealth) {
        globalGame.assetManager.get('playerhit.ogg').play();
      }
      _lastHealth = healthMapper[player].value;
      new Tween.to(this, HEALTH, 0.5)
        ..targetValues = [_lastHealth.toDouble()]
        ..easing = Linear.INOUT
        ..start(globalGame.tweenManager);
    }
    scoreElement.innerHtml = "Score: " + displayScore.round().toString();
    healthElement.innerHtml = "HP: " + displayHealth.round().toString();

    if(entities.length == 0) {
      if(!selecting) {
        if(globalGame.assetManager.get('boss.ogg').playing) {
          globalGame.assetManager.get('combat.ogg').loop();
          globalGame.assetManager.get('boss.ogg').stop();
        }
        PlayerControlSystem system = world.getSystem(PlayerControlSystem);
        system.stopped = true;
        healthMapper[player].value = 100.0;
        selecting = true;
        round++;
        showCards();
      }
    }
  }

  void showCards() {
    globalGame.assetManager.get('deal.ogg').play();
    for(int i = 0; i < 5; i++) {
      double sectionWidth = (1920 - 400) / 5;
      double x = 200 + i * sectionWidth + (sectionWidth - 263) / 2;
      double y = 1080 / 2 - 368 / 2;

      Card card = hand[i].getComponentByClass(Card);
      if(card.used) {
        card.setFromType(pickCard(round));
        StaticTexture texture = hand[i].getComponentByClass(StaticTexture);
        texture.texture = atlas[card.image];
      }

      Position position = hand[i].getComponentByClass(Position);
      position.xy = new Vector2(50.0, 50.0);
      new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
        ..delay = 1.0
        ..targetValues = [x, y]
        ..easing = Sine.INOUT
        ..start(globalGame.tweenManager);

      Scale scale = hand[i].getComponentByClass(Scale);
      scale.value = 0.5;
      new Tween.to(scale, Scale.SCALE, 0.75)
        ..delay = 1.0
        ..targetValues = [1.0]
        ..easing = Sine.INOUT
        ..start(globalGame.tweenManager);

      CardSelectSystem cardSelect = world.getSystem(CardSelectSystem);
      new Tween.call((n, b) => cardSelect.cardsDrawn = true)
        ..delay = 1.75
        ..start(globalGame.tweenManager);
    }
  }

  void redrawCard() {
    globalGame.assetManager.get('boost.ogg').play();
    for(int i = 0; i < 5; i++) {
      double sectionWidth = (1920 - 400) / 5;
      double x = 200 + i * sectionWidth + (sectionWidth - 263) / 2;
      double y = 1080 / 2 - 368 / 2;

      Card card = hand[i].getComponentByClass(Card);
      if(card.used) {
        card.setFromType(pickCard(round));
        StaticTexture texture = hand[i].getComponentByClass(StaticTexture);
        Position position = hand[i].getComponentByClass(Position);
        new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
          ..targetValues = [1920 - 263 * 0.5 - 50.0, 50.0]
          ..easing = Sine.INOUT
          ..start(globalGame.tweenManager);
        new Tween.call((n, b) {
          texture.texture = atlas[card.image];
          globalGame.assetManager.get('deal.ogg').play();})
          ..delay = 0.75
          ..start(globalGame.tweenManager);
        new Tween.set(position.vec, Vector2Accessor.XY)
          ..delay = 0.75
          ..targetValues = [50.0, 50.0]
          ..start(globalGame.tweenManager);
        new Tween.to(position.vec, Vector2Accessor.XY, 0.75)
          ..delay = 1.0
          ..targetValues = [x, y]
          ..easing = Sine.INOUT
          ..start(globalGame.tweenManager);

        CardSelectSystem cardSelect = world.getSystem(CardSelectSystem);
        new Tween.call((n, b) => cardSelect.cardsDrawn = true)
          ..delay = 1.75
          ..start(globalGame.tweenManager);

        Scale scale = hand[i].getComponentByClass(Scale);
        scale.value = 0.5;
        new Tween.to(scale, Scale.SCALE, 0.75)
          ..delay = 1.0
          ..targetValues = [1.0]
          ..easing = Sine.INOUT
          ..start(globalGame.tweenManager);
      }
    }
  }

  int get score => _score;
  void set score(int score) {
    this._score = score;
    new Tween.to(this, SCORE, 0.5)
      ..targetValues = [_score.toDouble()]
      ..easing = Linear.INOUT
      ..start(globalGame.tweenManager);
  }

  int getTweenableValues(Tween tween, int tweenType, List<num> returnValues) {
    if (tweenType == HEALTH) {
      returnValues[0] = displayHealth;
    } else if (tweenType == SCORE) {
      returnValues[0] = displayScore;
    }
    return 1;
  }

  void setTweenableValues(Tween tween, int tweenType, List<num> newValues) {
    if (tweenType == HEALTH) {
      displayHealth = newValues[0];
    } else if (tweenType == SCORE) {
      displayScore = newValues[0];
    }
  }

}