part of game;

class RenderSystem extends EntityProcessingSystem {

  GLWrapper gl;
  int width;
  int height;
  CanvasElement canvas;

  Framebuffer screenBuffer;

  SpriteBatch batch;
  PhysboxBatch debugBatch;
  List<Aabb2> renderLater = [];

  Camera2D fullscreen;
  Camera2D camera;

  List<Vector3> lightPos = [new Vector3(0.0, 0.0, 0.075)];
  List<Vector4> lightColor = [new Vector4(1.0, 1.0, 1.0, 1.0)];

  Vector4 ambientColor = new Vector4(1.0, 1.0, 1.0, 0.5);
  Vector3 attenuation = new Vector3(0.4, 3.0, 20.0);

  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Flip> flipMapper;
  Mapper<Scale> scaleMapper;
  Mapper<Rotation> rotationMapper;

  RenderSystem(this.gl, this.width, this.height, this.canvas) : super(Aspect.getAspectForAllOf([Position]).oneOf([StaticTexture, Animation, Hitbox])) {
    //batch = new SpriteBatch(gl, globalGame.assetManager.get('lighting'));
    batch = new SpriteBatch.defaultShader(gl);
    debugBatch = new PhysboxBatch.defaultShader(gl);
    resize(width, height, canvas);
  }

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    flipMapper = new Mapper<Flip>(Flip, world);
    scaleMapper = new Mapper<Scale>(Scale, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
  }

  @override
  void begin() {
    camera.update();
    fullscreen.update();

    gl.setGLViewport(width, height);

    screenBuffer.beginCapture();

    gl.clearScreen(Colors.deepSkyBlue);
    gl.context.disable(WebGL.DEPTH_TEST);
    gl.context.enable(WebGL.BLEND);
    gl.context.blendFunc(WebGL.SRC_ALPHA, WebGL.ONE_MINUS_SRC_ALPHA);

    batch.projection = camera.combined;
    batch.begin();
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];

    bool flipX, flipY = false;
    if(flipMapper.has(entity)) {
      flipX = flipMapper[entity].x;
      flipY = flipMapper[entity].y;
    }

    double scale = 1.0;
    if(scaleMapper.has(entity)) {
      scale = scaleMapper[entity].value;
    }

    double rot = 0.0;
    if(rotationMapper.has(entity)) {
      rot = rotationMapper[entity].value;
    }

    if(textureMapper.has(entity)) {
      batch.draw(textureMapper[entity].texture, position.x, position.y,
          flipX: flipX, flipY: flipY, scaleX: scale, scaleY: scale, angle: rot);
    }

    if(hitboxMapper.has(entity)) {
      renderLater.add(hitboxMapper[entity].box);
    }
  }

  @override
  void end() {
    //batch.setUniform('uLightPos', lightPos);
    //batch.setUniform('uLightColor', lightColor);
    //batch.setUniform('uAmbientLightColor', ambientColor);
    //batch.setUniform('uScreenRes', new Vector2(width.toDouble(), height.toDouble()));
    //batch.setUniform('uFalloff', attenuation);
    //batch.setUniform('uDiffuse', atlas[atlas.keys.first].bind(1));
    //batch.setUniform('uNormal', wallNorm.bind(2));

    batch.end();

    /*
    debugBatch.projection = camera.combined;
    debugBatch.begin();
    for(Aabb2 box in renderLater) {
      debugBatch.draw2D(box);
    }
    renderLater.clear();
    debugBatch.end();
    */

    screenBuffer.endCapture();

    gl.setGLViewport(canvas.width, canvas.height);

    screenBuffer.startRender(fullscreen.combined);
    //screenBuffer.setUniform("uTime", time);
    //screenBuffer.setUniform("uScreenRes", new Vector2(width.toDouble(), height.toDouble()));
    screenBuffer.endRender(0, 0, width, height);
  }

  resize(int width, int height, CanvasElement canvas) {
    this.width = width;
    this.height = height;
    this.canvas = canvas;

    screenBuffer = new Framebuffer(gl, width, height, filter: WebGL.LINEAR);
    fullscreen = new Camera2D.originBottomLeft(width, height);
    camera = new Camera2D.originBottomLeft(width, height);
  }

  Vector2 mouseToCanvas(Vector2 mouse) {
    var rect = canvas.getBoundingClientRect();
    return new Vector2((mouse.x - rect.left) * (width / canvas.width), (mouse.y - rect.top) * (height / canvas.height));
  }

}

WebGL.Texture linear(GLWrapper wrapper, ImageElement element) {
  var context = wrapper.context;

  WebGL.Texture texture = context.createTexture();
  context.bindTexture(WebGL.TEXTURE_2D, texture);
  context.pixelStorei(WebGL.UNPACK_FLIP_Y_WEBGL, 1);
  context.texImage2D(WebGL.TEXTURE_2D, 0, WebGL.RGBA, WebGL.RGBA,
      WebGL.UNSIGNED_BYTE, element);
  context.texParameteri(WebGL.TEXTURE_2D, WebGL.TEXTURE_MAG_FILTER, WebGL.LINEAR);
  context.texParameteri(WebGL.TEXTURE_2D, WebGL.TEXTURE_MIN_FILTER, WebGL.LINEAR);
  context.texParameteri(WebGL.TEXTURE_2D, WebGL.TEXTURE_WRAP_S, WebGL.CLAMP_TO_EDGE);
  context.texParameteri(WebGL.TEXTURE_2D, WebGL.TEXTURE_WRAP_T, WebGL.CLAMP_TO_EDGE);
  context.bindTexture(WebGL.TEXTURE_2D, null);
  return texture;
}