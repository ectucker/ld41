part of game;

class ShootingAISystem extends EntityProcessingSystem {

  Entity player;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<EnemyData> enemyMapper;
  Mapper<Rotation> rotationMapper;

  double speed = 200.0;
  double fireTime = -1.0;

  ShootingAISystem(this.player) : super(Aspect.getAspectForAllOf([ShootingEnemyAI, Velocity, Position, Hitbox, Rotation])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    enemyMapper = new Mapper<EnemyData>(EnemyData, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Velocity velocity = velocityMapper[entity];
    Position position = positionMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];
    Position playerPos = positionMapper[player];
    EnemyData enemyData = enemyMapper[entity];

    Vector2 direction = playerPos.vec.clone();
    direction.sub(hitbox.box.center.clone());

    if(fireTime == -1.0) {
      fireTime = enemyData.attackrate;
    }

    fireTime -= world.delta;

    if(direction.length < 600.0) {
      if(fireTime <= 0.0) {
        direction.normalize();
        direction.scale(enemyData.speed * 300.0);

        makeLightning(world, hitbox.box.center.clone(), direction, enemyData.damage, atan2(direction.y, direction.x) + PI);

        fireTime = enemyData.attackrate;
      }
      velocity.xy = new Vector2.all(0.0);
    } else {
      direction.normalize();
      direction.scale(enemyData.speed * 400.0);
      velocity.xy = direction;
    }

    rotationMapper[entity].value = atan2(direction.y, direction.x) + PI;
  }

}