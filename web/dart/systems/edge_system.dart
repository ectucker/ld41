part of game;

class EdgeStopSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;

  EdgeStopSystem() : super(Aspect.getAspectForAllOf([Position, Hitbox])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Position position = positionMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];

    double width = render.width.toDouble();
    double height = render.height.toDouble();
    Vector2 min = hitbox.box.min;
    Vector2 max = hitbox.box.max;

    if(min.x < 0) {
      position.x = 0.0;
    }
    if(min.y < 0) {
      position.y = 0.0;
    }
    if(max.x > width) {
      position.x = width - hitbox.width;
    }
    if(max.y > height) {
      position.y = height - hitbox.height;
    }
  }

}

class EdgeDeleteSystem extends EntityProcessingSystem {

  Mapper<Hitbox> hitboxMapper;

  EdgeDeleteSystem() : super(Aspect.getAspectForAllOf([EdgeDelete, Hitbox])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Hitbox hitbox = hitboxMapper[entity];

    double width = render.width.toDouble();
    double height = render.height.toDouble();
    Vector2 min = hitbox.box.min;
    Vector2 max = hitbox.box.max;

    if(min.x < -400) {
      world.deleteEntity(entity);
    } else if(min.y < -400) {
      world.deleteEntity(entity);
    } else if(max.x > width + 400) {
      world.deleteEntity(entity);
    } else if(max.y > height + 400) {
      world.deleteEntity(entity);
    }
  }

}