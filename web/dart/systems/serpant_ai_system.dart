part of game;

class SerpantAISystem extends EntityProcessingSystem {

  Entity player;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Health> healthMapper;
  Mapper<EnemyData> enemyMapper;
  Mapper<Flip> flipMapper;

  double fireTime = -1.0;
  int aiDir = 1;

  SerpantAISystem(this.player) : super(Aspect.getAspectForAllOf([SerpantAI, Velocity, Position, Hitbox, EnemyData, Flip])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    healthMapper = new Mapper<Health>(Health, world);
    enemyMapper = new Mapper<EnemyData>(EnemyData, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Velocity velocity = velocityMapper[entity];
    Position position = positionMapper[entity];
    EnemyData enemyData = enemyMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];
    Position playerPos = positionMapper[player];

    Vector2 direction = playerPos.vec.clone();
    direction.sub(hitbox.box.center.clone());

    if(fireTime == -1.0) {
      fireTime = enemyData.attackrate;
    }

    fireTime -= world.delta;
    if(direction.length < 90.0) {
      if(fireTime <= 0.0) {
        healthMapper[player].value -= enemyData.damage;
        fireTime = enemyData.attackrate;
      }
    }

    if(aiDir < 0 && hitbox.box.center.x < 0) {
      aiDir = 1;
    } else if(hitbox.box.center.x > 1920) {
      aiDir = 0;
    }
    flipMapper[entity].x = aiDir < 0;

    velocity.xy = new Vector2(enemyData.speed * aiDir, 0.0);
  }

}