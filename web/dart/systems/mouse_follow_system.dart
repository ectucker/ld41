part of game;

class MouseFollowSystem extends EntityProcessingSystem {

  Mouse mouse;
  Mapper<Position> positionMapper;

  MouseFollowSystem(this.mouse) : super(Aspect.getAspectForAllOf([FollowMouse, Position])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Position position = positionMapper[entity];

    position.xy = render.mouseToCanvas(new Vector2(mouse.x.toDouble(), mouse.y.toDouble()));
  }

}