part of game;

class SharkAISystem extends EntityProcessingSystem {

  Entity player;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Health> healthMapper;
  Mapper<EnemyData> enemyMapper;
  Mapper<Flip> flipMapper;

  double fireTime = -1.0;
  double spawnTime = 4.0;
  int aiDir = 1;

  SharkAISystem(this.player) : super(Aspect.getAspectForAllOf([SharkAI, Velocity, Position, Hitbox, EnemyData, Flip])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    healthMapper = new Mapper<Health>(Health, world);
    enemyMapper = new Mapper<EnemyData>(EnemyData, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    RenderSystem render = world.getSystem(RenderSystem);
    Velocity velocity = velocityMapper[entity];
    Position position = positionMapper[entity];
    EnemyData enemyData = enemyMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];
    Position playerPos = positionMapper[player];

    Vector2 direction = playerPos.vec.clone();
    direction.sub(hitbox.box.center.clone());

    if(fireTime == -1.0) {
      fireTime = enemyData.attackrate;
    }

    fireTime -= world.delta;
    if(direction.length < 180.0) {
      if(fireTime <= 0.0) {
        healthMapper[player].value -= enemyData.damage;
        fireTime = enemyData.attackrate;
      }
    }

    spawnTime -= world.delta;
    if(spawnTime <= 0.0) {
      makeRemora(world, position.x);
      spawnTime = 3.5;
    }

    if(aiDir < 0 && hitbox.box.center.x < 0) {
      aiDir = 1;
    } else if(hitbox.box.center.x > 1920) {
      aiDir = -1;
    }
    flipMapper[entity].x = aiDir > 0;

    velocity.xy = new Vector2(enemyData.speed * 300.0 * aiDir, 0.0);
  }

}

Entity makeRemora(var world, double x) {
  var enemyData = enemytypes['sharkMinion'];
  Entity enemy = world.createEntity();
  enemy.addComponent(new Position(x, -300));
  enemy.addComponent(new StaticTexture(atlas[enemyData['image'] + "1"]));
  enemy.addComponent(new Hitbox(enemyData['hitbox'][0], enemyData['hitbox'][1],
      enemyData['hitbox'][2], enemyData['hitbox'][3]));
  enemy.addComponent(new Collision());
  enemy.addComponent(new Velocity(0.0, 0.0));
  enemy.addComponent(new Health(enemyData[1]['hp'], false));
  enemy.addComponent(new PointValue(enemyData[1]['points']));
  enemy.addComponent(new Enemy());
  enemy.addComponent(new EnemyData.fromMap(enemyData[1]));
  enemy.addComponent(new BasicEnemyAI());
  enemy.addComponent(new Rotation.none());
  enemy.addComponent(new Animation(enemyData['image'], enemyData['frames'], enemyData['duration']));
  enemy.addToWorld();
}