part of game;

class AnimationSystem extends EntityProcessingSystem {

  Mapper<Animation> animationMapper;
  Mapper<StaticTexture> textureMapper;

  AnimationSystem() : super(Aspect.getAspectForAllOf([Animation, StaticTexture])) {}

  @override
  void initialize() {
    animationMapper = new Mapper<Animation>(Animation, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
  }

  @override
  void processEntity(Entity entity) {
    Animation animation = animationMapper[entity];
    StaticTexture texture = textureMapper[entity];

    animation.frameTime -= world.delta;
    if(animation.frameTime <= 0) {
      animation.frameTime = animation.duration;
      animation.frame++;
      if(animation.frame > animation.frames) {
        animation.frame = 1;
      }

      texture.texture = atlas[animation.base + animation.frame.toString()];
    }
  }

}