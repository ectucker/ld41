part of game;

class DeathSystem extends EntityProcessingSystem {

  Mapper<Health> healthMapper;
  Mapper<PointValue> pointMapper;

  DeathSystem() : super(Aspect.getAspectForAllOf([Health])) {}

  @override
  void initialize() {
    healthMapper = new Mapper<Health>(Health, world);
    pointMapper = new Mapper<PointValue>(PointValue, world);
  }

  @override
  void processEntity(Entity entity) {
    RoundControlSystem rounds = world.getSystem(RoundControlSystem);

    Health health = healthMapper[entity];

    if(health.value <= 0.0) {
      if(pointMapper.has(entity)) {
        rounds.score += pointMapper[entity].value;
      }
      if(entity.getComponentByClass(PlayerControl) != null) {
        globalGame.assetManager.get('combat.ogg').stop();
        globalGame.assetManager.get('boss.ogg').stop();
        globalGame.pushState(new EndMenuState(globalGame, rounds.score));
      } else {
        entity.deleteFromWorld();
      }
    }
  }

}