part of game;

class CollisionRemoveSystem extends EntityProcessingSystem {

  Mapper<Collision> collisionMapper;
  Mapper<Health> healthMapper;
  Mapper<Damage> damageMapper;

  CollisionRemoveSystem() : super(Aspect.getAspectForAllOf([Collision, RemoveOnCollision, Damage])) {}

  @override
  void initialize() {
    collisionMapper = new Mapper<Collision>(Collision, world);
    healthMapper = new Mapper<Health>(Health, world);
    damageMapper = new Mapper<Damage>(Damage, world);
  }

  @override
  void processEntity(Entity entity) {
    Collision collision = collisionMapper[entity];

    for(var other in collision.others) {
      if(healthMapper.has(other)) {
        if(healthMapper[other].player != damageMapper[entity].player) {
          entity.deleteFromWorld();
        }
      }
    }
  }

}