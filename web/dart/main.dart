library game;

import 'dart:html';
import 'package:cobblestone/cobblestone.dart';
import 'package:dartemis/dartemis.dart';
import 'package:yaml/yaml.dart' as YAML;
import 'package:tweenengine/tweenengine.dart';
import 'dart:web_gl' as WebGL;

part 'game.dart';
part 'game_state.dart';
part 'entities.dart';

part 'enemies/card.dart';
part 'enemies/enemy_templates.dart';

part 'menu/menu.dart';
part 'menu/main_menu.dart';
part 'menu/game_menu.dart';
part 'menu/instruction_menu.dart';
part 'menu/end_menu.dart';

part 'components/flags.dart';
part 'components/flip.dart';
part 'components/hitbox.dart';
part 'components/static_texture.dart';
part 'components/vector.dart';
part 'components/collision.dart';
part 'components/health.dart';
part 'components/enemydata.dart';
part 'components/animation.dart';

part 'systems/camera_center_system.dart';
part 'systems/hitbox_position_system.dart';
part 'systems/movement_system.dart';
part 'systems/render_system.dart';
part 'systems/player_control_system.dart';
part 'systems/edge_system.dart';
part 'systems/basic_ai_system.dart';
part 'systems/collision_list_system.dart';
part 'systems/damage_system.dart';
part 'systems/death_system.dart';
part 'systems/collision_remove_system.dart';
part 'systems/mouse_follow_system.dart';
part 'systems/card_select_system.dart';
part 'systems/round_control_system.dart';
part 'systems/shooting_ai_system.dart';
part 'systems/animation_system.dart';
part 'systems/shark_ai_system.dart';
part 'systems/serpant_ai_system.dart';
part 'systems/cthulu_ai_system.dart';

Game globalGame;

void main() {
  globalGame = new Game();
}

loadYaml(String url) async {
  return YAML.loadYaml(await HttpRequest.getString(url));
}
