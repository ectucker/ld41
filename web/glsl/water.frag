// Taken from https://www.shadertoy.com/view/MsKXzD
precision highp float;

#define speed 1.0

#define xDistMag 0.001
#define yDistMag 0.001

#define xSineCycles 12.56
#define ySineCycles 12.56

uniform sampler2D uTexture;
uniform vec4 uColor;

uniform vec2 uScreenRes;

uniform float uTime;

varying vec2 vTextureCoord;

void main(void) {
    float minRes = min(uScreenRes.x / 1920.0, uScreenRes.y / 1080.0);

    vec2 texCoord = vTextureCoord / minRes;

    float time = uTime * speed;
    float xAngle = time + texCoord.y * ySineCycles;
    float yAngle = time + texCoord.x * xSineCycles;
    
    vec2 distortOffset = 
        vec2(sin(xAngle), sin(yAngle)) *
        vec2(xDistMag, yDistMag);

    texCoord += distortOffset;

    gl_FragColor = texture2D(uTexture, texCoord);
}